import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Menu } from './menus/entities/menu.entity';
import { MenusModule } from './menus/menus.module';
import { Serve } from './serve/entities/serve.entity';
import { ServeModule } from './serve/serve.module';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'topkung493.ddns.net',
      port: 3306,
      username: 'LazyBunnies',
      password: 'Lazy@212317',
      database: 'project_lazy',
      entities: [Menu, User, Table, Serve],
      synchronize: true,
    }),
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   logging: false,
    //   entities: [Menu],
    //   migrations: [],
    //   subscribers: [],
    // }),
    MenusModule,
    TablesModule,
    UsersModule,
    AuthModule,
    ServeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
